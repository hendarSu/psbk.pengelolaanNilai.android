package info.androidhive.Nilai.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import info.androidhive.Nilai.R;
import info.androidhive.Nilai.model.Nilai;

public class NilaiAdapter extends RecyclerView.Adapter<NilaiAdapter.NilaiViewHolder> {

    private List<Nilai> nilais;
    private int rowLayout;
    private Context context;




    public static class NilaiViewHolder extends RecyclerView.ViewHolder {
        LinearLayout moviesLayout;
        TextView movieTitle;
        TextView data;
        TextView movieDescription;
        TextView rating;


        public NilaiViewHolder(View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
            movieTitle = (TextView) v.findViewById(R.id.title);
            data = (TextView) v.findViewById(R.id.subtitle);
            movieDescription = (TextView) v.findViewById(R.id.description);
            rating = (TextView) v.findViewById(R.id.rating);
        }
    }

    public NilaiAdapter(List<Nilai> nilais, int rowLayout, Context context) {
        this.nilais = nilais;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public NilaiAdapter.NilaiViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new NilaiViewHolder(view);
    }


    @Override
    public void onBindViewHolder(NilaiViewHolder holder, final int position) {
        holder.movieTitle.setText(nilais.get(position).getGrade());
        holder.data.setText(nilais.get(position).getMatkul());
        holder.movieDescription.setText(nilais.get(position).getNilai());
        holder.rating.setText(nilais.get(position).getNrp());
    }

    @Override
    public int getItemCount() {
        return nilais.size();
    }
}
