package info.androidhive.Nilai.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import info.androidhive.Nilai.R;
import info.androidhive.Nilai.adapter.NilaiAdapter;
import info.androidhive.Nilai.model.Nilai;
import info.androidhive.Nilai.model.NilaiResponse;
import info.androidhive.Nilai.rest.ApiClient;
import info.androidhive.Nilai.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainService extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    // TODO - insert your themoviedb.org API KEY here
//    private final static String API_KEY = "3445345345";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_service);

//        if (API_KEY.isEmpty()) {
//            Toast.makeText(getApplicationContext(), "Please obtain your API KEY first from themoviedb.org", Toast.LENGTH_LONG).show();
//            return;
//        }

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<NilaiResponse> call = apiService.getAllNilai("allNilai");
        call.enqueue(new Callback<NilaiResponse>() {
            @Override
            public void onResponse(Call<NilaiResponse> call, Response<NilaiResponse> response) {
                int statusCode = response.code();
                List<Nilai> nilais = response.body().getResults();
                recyclerView.setAdapter(new NilaiAdapter(nilais, R.layout.list_item_movie, getApplicationContext()));
            }

            @Override
            public void onFailure(Call<NilaiResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
            }
        });
    }
}
