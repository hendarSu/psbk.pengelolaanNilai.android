package info.androidhive.Nilai.rest;

/**
 * Created by HTWO on 5/14/2016.
 */
import info.androidhive.Nilai.model.NilaiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
//    @GET("movie/top_rated")
//    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);
//
//    @GET("movie/{id}")
//    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);

    @GET("Nilai/{allNilai}")
    Call<NilaiResponse> getAllNilai(@Path("allNilai") String allNilai);
}