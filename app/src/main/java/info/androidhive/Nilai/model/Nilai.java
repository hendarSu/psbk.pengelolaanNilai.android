package info.androidhive.Nilai.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HTWO on 5/14/2016.
 */
public class Nilai {
    @SerializedName("kode_matkul")
    private String kodeMatakuliah;
    @SerializedName("id_nilai")
    private int idNilai;
    @SerializedName("nilai")
    private String nilai;
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("grade")
    private String grade;
    @SerializedName("semester")
    private String semester;
    @SerializedName("sks")
    private int sks;
    @SerializedName("nrp")
    private String nrp;
    @SerializedName("matkul")
    private String matkul;

    public Nilai(String kodeMatakuliah, int idNilai, String nilai, String kelas, String grade, String semester, int sks, String nrp, String matkul) {
        this.kodeMatakuliah = kodeMatakuliah;
        this.idNilai = idNilai;
        this.nilai = nilai;
        this.kelas = kelas;
        this.grade = grade;
        this.semester = semester;
        this.sks = sks;
        this.nrp = nrp;
        this.matkul = matkul;
    }

    public String getKodeMatakuliah() {
        return kodeMatakuliah;
    }

    public void setKodeMatakuliah(String kodeMatakuliah) {
        this.kodeMatakuliah = kodeMatakuliah;
    }

    public int getIdNilai() {
        return idNilai;
    }

    public void setIdNilai(int idNilai) {
        this.idNilai = idNilai;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public String getNrp() {
        return nrp;
    }

    public void setNrp(String nrp) {
        this.nrp = nrp;
    }

    public String getMatkul() {
        return matkul;
    }

    public void setMatkul(String matkul) {
        this.matkul = matkul;
    }
}
